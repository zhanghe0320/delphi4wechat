program weixin;

uses
  Forms,
  main in 'main.pas' {Form1},
  webapp in 'webapp.pas',
  HyperLinksDecorator in 'HyperLinksDecorator.pas',
  RegExpr in 'RegExpr.pas',
  hashtable in 'hashtable.pas',
  backend in 'backend.pas',
  superobject in 'superobject.pas',
  log4me in 'log4me.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
